<?php
    include('session.php');
    require 'database.php';

    $sql = "SELECT
              *
            FROM
              tbl_user
            WHERE
              tbl_user.user_id = ".$_SESSION['user_id'].";";

    $result = mysqli_query($conn, $sql);
    // Fetch Records
    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_array($result, MYSQL_NUM)) { 
            $user_id = $row[0];
            $username = $row[1];
            // $password = $row[2];
            $firstName = $row[3];
            $lastName = $row[4];
            $address = $row[5];
            $age = $row[6];
            $dob = $row[7];
            $accountNumber = $row[8];
        }
    }    

    $sql = "SELECT
              *
            FROM
              tbl_bank_account
            WHERE
              tbl_bank_account.user_id = ".$_SESSION['user_id'].";";

    $result = mysqli_query($conn, $sql);
    // Fetch Records
    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_array($result, MYSQL_NUM)) { 
            $bankAccount_id = $row[0];
            $user_id = $row[1];
            $balance = $row[2];
        }
    }     

    $totalPendingAmount = 0; 
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>CP | Account Information</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
  <script src="../../plugins/jQuery/jQuery-2.2.0.min.js"></script>
   <script type="text/javascript">
        $('#okBal').hide();
        $('#noBal').hide();
 </script>
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
     <header class="main-header">
          <nav class="navbar navbar-static-top" role="navigation">
          <div class="navbar-custom-menu">
               <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs"><?php echo $firstName." ".$lastName; ?></span>
                         </a>
                         <ul class="dropdown-menu">
                             <li class="user-footer">
                             <div class="pull-right">
                                <a href="../../logout.php" class="btn btn-default btn-flat">Log out</a>
                             </div>
                             </li>
                         </ul>
                    </li>
               </ul>
          </div>
          </nav>
     </header>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
     <a href="profile.php"><b>Cashless Payment</b></a>
    </h1>
</section>
<!-- Main content -->
<section class="content">
 <div class="row">
   <div class="col-md-4">
     <!-- Profile Image -->
     <div class="box box-primary">
       <div class="box-body box-profile">
         <h3 class="profile-username text-center"><?php echo $firstName." ".$lastName; ?></h3>
         <ul class="list-group list-group-unbordered">
        <li class="list-group-item">
             <b>Username</b> <a class="pull-right"><?php echo $username; ?></a>
        </li>
        <li class="list-group-item">
             <b>Address</b> <a class="pull-right"><?php echo $address; ?></a>
        </li>
        <li class="list-group-item">
             <b>D.O.B.</b> <a class="pull-right"><?php echo $dob; ?></a>
        </li>
        <li class="list-group-item">
             <b>Age</b> <a class="pull-right"><?php echo $age; ?></a>
        </li>
   </ul>
</div>
</div>
</div>
<div class="col-md-8">
    <div class="row">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab">Account Information</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="activity">
                    <li class="list-group-item">
                        <b>Account Number</b> <a class="pull-right"><?php echo $accountNumber; ?></a>
                    </li>
                    <li class="list-group-item">
                        <b>Account Name</b> <a class="pull-right"><?php echo $firstName." ".$lastName; ?></a>
                    </li>
                    <li class="list-group-item">
                        <b>Account Balance</b><a class="pull-right"><b>PHP <?php echo $balance; ?></b></a>
                    </li>
                    <br><br>
                    <form class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-4"></div>  
                            <div class="col-sm-4">
                            <a href="amount.php" id="okBal" class="btn btn-block btn-lg btn-primary"><b>Cashless Withdraw</b></a>
                            <div id="noBal" class="callout callout-danger">
                                <h4 >Your balance is insufficient.</h4>
                            </div>
                            </div>  
                            <div class="col-sm-4"></div>  
                        </div>
                    </form>
                </div>
            </div>
        </div>  
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Pending Serial Numbers</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Serial #</th>
                        <th>Action</th> 
                    </tr>
                </thead>              
                <tbody>
                <?php 
                    require 'database.php';
                    $sql = "SELECT
                              *
                            FROM
                              tbl_serial;";
                                                                                    
                    // echo $sql;
                    $result = mysqli_query($conn, $sql);
                    if (mysqli_num_rows($result) > 0) {
                        while($row = mysqli_fetch_array($result, MYSQL_NUM)) { 
                            $serial_id = $row[0];
                            $user_id = $row[1];
                            $serialNumber = $row[2];
                            $amount = $row[3];
                            $pending = $row[4];
                            $totalPendingAmount += $amount;
                ?>
                    <tr>
                        <td><?php echo $serialNumber; ?></td>
                        <td>PHP <?php echo $amount; ?></td>
                        <td class="text-center">
                            <a href="removeSerial.php?serial_id=<?php echo $serial_id; ?>" class="btn btn-danger btn-xs">Cancel</a>
                        </td>
                    </tr>
                <?php 
                        }
                    }
                        mysqli_close($conn);
                ?>                                          
                </tbody>                
              </table>
            </div>
            <!-- /.box-body -->
          </div><br><br><br><br>
    </div>
</div>
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<script src="../../plugins/fastclick/fastclick.js"></script>
<script src="../../dist/js/app.min.js"></script>
<script src="../../dist/js/demo.js"></script>
</body>
</html>

 <script type="text/javascript">
    $( document ).ready(function() {
        var totalPendingAmount = <?php echo $totalPendingAmount; ?>;
        var balance = <?php echo $balance; ?>;

        if(totalPendingAmount > balance){
            $('#okBal').remove();
            $('#noBal').show();
        }else {
            $('#okBal').show();
            $('#noBal').remove();
        }
    });
 </script>
