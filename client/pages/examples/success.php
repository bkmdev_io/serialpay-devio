<?php
    include('session.php');
    require 'database.php';

    $sql = "SELECT
              *
            FROM
              tbl_user
            WHERE
              tbl_user.user_id = ".$_SESSION['user_id'].";";

    $result = mysqli_query($conn, $sql);
    // Fetch Records
    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_array($result, MYSQL_NUM)) { 
            $user_id = $row[0];
            $username = $row[1];
            // $password = $row[2];
            $firstName = $row[3];
            $lastName = $row[4];
            $address = $row[5];
            $age = $row[6];
            $dob = $row[7];
            $accountNumber = $row[8];
        }
    }    

    if(isset($_GET['serial'])){
      $serial = $_GET['serial'];
    } else {
      echo "<script>window.location.href = 'profile.php'</script>";
      
    }

    $sql = "SELECT
              *
            FROM
              tbl_bank_account
            WHERE
              tbl_bank_account.user_id = ".$_SESSION['user_id'].";";

    $result = mysqli_query($conn, $sql);
    // Fetch Records
    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_array($result, MYSQL_NUM)) { 
            $bankAccount_id = $row[0];
            $user_id = $row[1];
            $balance = $row[2];
        }
    }     
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>CP | Account Information</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="../../dist/css/check.scss">

</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
     <header class="main-header">
          <nav class="navbar navbar-static-top" role="navigation">
          <div class="navbar-custom-menu">
               <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs"><?php echo $firstName." ".$lastName; ?></span>
                         </a>
                         <ul class="dropdown-menu">
                             <li class="user-footer">
                             <div class="pull-right">
                                <a href="../../logout.php" class="btn btn-default btn-flat">Log out</a>
                             </div>
                             </li>
                         </ul>
                    </li>
               </ul>
          </div>
          </nav>
     </header>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
   <h1>
     <a href="profile.php"><b>Cashless Payment</b></a>
    </h1>
</section>
<!-- Main content -->
<section class="content">
 <div class="row">
   <div class="col-md-4">
     <!-- Profile Image -->
     <div class="box box-primary">
       <div class="box-body box-profile">
         <h3 class="profile-username text-center"><?php echo $firstName." ".$lastName; ?></h3>
         <ul class="list-group list-group-unbordered">
        <li class="list-group-item">
             <b>Username</b> <a class="pull-right"><?php echo $username; ?></a>
        </li>
        <li class="list-group-item">
             <b>Address</b> <a class="pull-right"><?php echo $address; ?></a>
        </li>
        <li class="list-group-item">
             <b>D.O.B.</b> <a class="pull-right"><?php echo $dob; ?></a>
        </li>
        <li class="list-group-item">
             <b>Age</b> <a class="pull-right"><?php echo $age; ?></a>
        </li>
   </ul>
</div>
</div>
</div>
<div class="col-md-8">
    <div class="row">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab">Success</a></li>
            </ul>
            <div class='my-fancy-container'>
			<div class="col-md-6"></div>
			
		
    <span style="font-size:6em;" class='my-icon fa fa-check-circle-o'> </span><h1 class="wew" align="center">&nbsp &nbsp &nbsp Success</h1>
	<br>
  <form>
	<label >
             12-Digit Serial Number
              
            </label>
			<input type="text" class="form-control" placeholder="Serial" readonly value="<?php echo $serial; ?>"><br>
    <div class="form-group">
    <div class="col-sm-4"></div>  
    <div class="col-sm-4">
    <a href="profile.php" class="btn btn-block btn-lg btn-success"><b>Home</b></a>
    </div>  
    <div class="col-sm-4"></div>  
    </div>          
    <br><br><br>      
			
	
	</form>
  <br><br>
</div>

		
          <style>
		  .my-icon {
    vertical-align: middle;
    font-size: 40px;
	color: green;

}
.wew{
font-size: 50px;
color:green;
}
.my-fancy-container {

    margin: 60px;
    padding: 10px;
}</style>
        </div>  
    </div>		  
</div>
<script src="../../plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<script src="../../plugins/fastclick/fastclick.js"></script>
<script src="../../dist/js/app.min.js"></script>
<script src="../../dist/js/demo.js"></script>
</body>
</html>
